ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.1.3"
//ThisBuild / scalaVersion := "2.13.10"
lazy val root = (project in file("."))
  .settings(
    name := "word-storage",
    resolvers += "mvn proxy" at "https://nexus.tcsbank.ru/repository/mvn-maven-proxy",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
    Compile / mainClass := Some(
      "ru.tinkoff.backendacademy.wordstorage.ConsoleInMemory"
    ),
    coverageFailOnMinimum := true,
    coverageMinimumStmtTotal := 60
  )
  .enablePlugins(JavaAppPackaging)

scalacOptions ++= Seq("-Werror")
